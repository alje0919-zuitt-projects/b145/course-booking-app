import { useState , useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	// use the state hook for this component to be able to store its state
	/*
		syntax:
			const [getter, setter] = useState(initialGetterValue)

	*/

	/*const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);


	function enroll() {
		if(seats > 0) {
			setCount(count + 1)
			setSeats(seats - 1)
			console.log(`Enrolless: ${count}`)
		} else {
			alert("No more seats available!")
		}
	}*/


	// console.log(props.courseProp);
	const {name, description, price, _id} = courseProp

	return(

		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>P{price}</Card.Text>
		
				<Button variant="danger" as={Link} to={`/courses/${_id}`}>Details</Button>
			</Card.Body>
		</Card>
		)
}