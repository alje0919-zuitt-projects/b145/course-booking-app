import { Fragment, useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';


export default function Courses() {

	const [course, setCourses] = useState([])
	// console.log(coursesData[0])
	// map method
	// key={} is used as a unique identifier
	/*const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course} />
			)
	})*/


	useEffect(() => {
		fetch('http://localhost:4000/api/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
					)
			}))
		})
	}, [])

	return(
		<Fragment>
			<h1>Courses</h1>
			{/*<CourseCard courseProp={coursesData[0]} />*/}
			{course}
		</Fragment>
		)
}